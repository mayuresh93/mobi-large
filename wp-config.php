<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mobi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Sq~9eh}Gdu Y, #TV+kg6F<<#/DEtyv8PaWMOwrIo[#5Vbe5<kK>:ep2L5]4QzlH');
define('SECURE_AUTH_KEY',  '!0P{lJ=MuO@v;b+5aoQvPEL271Y~Td`?{;~M 3p&&q&:F:},bVHXY:ecV,Y{ZU&~');
define('LOGGED_IN_KEY',    ')`>3C2~thvuJfyK ~w4PIQ})J[*r~;FH<qa? u@0XU87GR>k=TeFTUV<1HEi%XC1');
define('NONCE_KEY',        '0enJV+pd/@{Naw[~>{C@@}Uemao }K%R(x{!6fzpXoA7JvVof*~$gzGpe:K` !2:');
define('AUTH_SALT',        '2}i/u#LN(p<-cT,MnNbhM]AoZ[{2&IOz=>l(vr|mCtn#FC^)TtihbodYG=vk3$6{');
define('SECURE_AUTH_SALT', 'x>28+nkiM(@} 7La{X% ~]{(zZ<+5t0M ;c&wLzQ ]; P7]|bV,IJxD/Uc:@Y4P*');
define('LOGGED_IN_SALT',   '7FKL2:N9 y8K11Ld,1_v-R+|MDMui5b4ihINK_`FQ@BhNoy]I{fl38t&lSg;M0xC');
define('NONCE_SALT',       'lfm?PY;*v63^}euFBSxfU2_Wr2EXPaecem4[g&kSKQ4J<W?_Zz@4pWHPX0f-9A(M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
